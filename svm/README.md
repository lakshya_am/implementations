# Support Vector Machines
[[_TOC_]]

## The Maximal Margin Classifier

__Objective: *Find a separating hyperplane which is farthest from the training examples between two perfectly separated classes.*__

<img src="images/image1.jpeg" alt="Drawing" class="center" width="500">

From above figure, for any decision boundary which separates two classes we can write:

```math
y_i (\beta_0 + \beta_1 x_1 + \beta_2 x_2) > 0   \hspace{8cm} (1)
```
$`\forall i \in \{1, 2, 3, \cdots, n\}`$ where $`n`$ is the number of training examples and $`y_i = -1, 1`$ are the class labels. 

We can generalize this for any $`p`$-dimensional feature space:
```math
\begin{gathered}
y_i (\beta_0 + \beta_1 x_1 + \beta_2 x_2 + \cdots + \beta_p x_p) > 0  \qquad \mathrm{or}, \\
y_i (x^{T} \beta + \beta_0) > 0
\end{gathered} 
```

__How to find the separating hyperplane?__

1. Compute the perpendicular distance of each training example from given hyperplane
2. The smallest of those distances is the minimal distance from the training examples from the hyperplane and is called __margin__
3. Find the hyperplane with the largest margin

We will work for the case of only two features to see what we meant.

<img src="images/image2.jpeg" alt="Drawing" align=center width="500"/>

As seen from above figure, the two classes are exactly separable with a hyperplane (a line) which is given by the solution of the optimization problem as $`x^T \beta + \beta_0 = 0`$. The band in the figure is $`M`$ units away from the hyperplane on either side, and hence $`2M`$ units wide. This band is called the __margin__. As we can see in the figure, it is the shortest perpendicular distance from the hyperplane to the training examples. The training examples which lie on the margin boundaries are called __support vectors__.

The optimization condition for the Maximal Margin Classifier requires to maximize the margin between two classes. But it rarely happens that two classes can be exactly seperable in real world data. Hence for the case when two classes are not _exactly_ separable, no maximal margin classifier exists. For the non-separable classes, we have to use a __support vector classifier.__

## Support Vector Classifier
Now suppose the classes are non-separable and there is some class overlap in the feature space. One way to deal with this is to still maximize $`M`$ while allowing some training examples to be wrongly classified. This would add robustness to the classifier since Maximal Margin classifier is extremely sensitive to change in the training examples near the margin boundary.

<img src="images/image3.jpeg" alt="Drawing" align=center width="600"/>

Here we can see the case of class overlap. Again, the decision boundary is the heavy solid line while the lighter solid lines enclose the region of maximal margin of width $`2M = 2 / \mid\mid\beta \mid \mid`$. 
- We define something called as _slack_ variables which tell us how much does each training example violate the margin
- The data points labeled $`\epsilon_{i}^{*}`$ are on the wrong side on margin by an amount $`\epsilon_{i}^{*} = M \epsilon_{i}`$
- The points on the correct side of the margin will have $`\epsilon_{i}^{*} = 0`$ (_no violation_)
- If $`0 < \epsilon_{i} < 1`$ this implies $`0 < \epsilon_{i}^{*} < M`$. This means the training example is on the wrong side of the margin
- If $`\epsilon_{i} > 1`$ this implies $`\epsilon_{i}^{*} > M`$. This means the training example is on the wrong side of the hyperplane

This whole idea can be represented mathematically by the optimization problem:
```math
\begin{gathered}
    \min \mid\mid\beta\mid\mid \mathrm{subject \;  to} 
    \begin{cases} 
    y_i (x_i^T \beta + \beta_0) \geq 1 - \epsilon_i \; \forall i, \\
    \epsilon_i \geq 0,\; \sum \epsilon_i \leq \mathrm{constant}
    \end{cases}
\end{gathered}
```

Computationally, the above equation can be written as
```math 
\begin{gathered}
    \min_{\beta,\, \beta_0} \frac{1}{2} \mid\mid\beta\mid\mid^2 + \, C \sum_{i=0}^{n} \epsilon_i  \\
    \mathrm{subject \: to \:} \epsilon_i \geq 0,\; y_i (x_i^T \beta + \beta_0) \geq 1 - \epsilon_i \; \forall i  
\end{gathered}
```

$`C`$ is a hyperparameter called the _cost parameter_. The case of separable classes (no class overlap) corresponds to $`C = \infty`$. The following can be interpreted about parameter $`C`$:
- It can be seen from Eq.(4) that the lower values of $`C`$ will make the margin larger and vice-versa. This makes sense since lowering $`C`$ will increase $`\sum_{i} \epsilon_i`$ (letting more training examples violating the margin)
- Hence larger values of C focus attention more on (correctly classified) points near the decision boundary, while smaller values involve data further away. 
- Parameter $`C`$ can be thought of as a regularization parameter, where higher value of $`C`$ overfits the data (_more restrictive of margin violations_) and hence results in high variance and low bias. Similarly, lower value of $`C`$ underfits the data (high bias and low variance)

<div>
    <img src="images/image4.jpeg" alt="Drawing" align=center width="600"/>
</div>

<div>
    <img src="images/image5.jpeg" alt="Drawing" align=center width="600"/>
</div>

In both the above figures, the solid dots refer to the support vectors which fall exactly on the margin ($`\epsilon_i = 0`$).

## Classification with Non-linear Decision Boundaries and Support Vector Machines
