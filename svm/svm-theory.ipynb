{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Support Vector Machines"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The Maximal Margin Classifier\n",
    "\n",
    "__Objective: *Find a separating hyperplane which is farthest from the training examples between two perfectly separated classes.*__\n",
    "\n",
    "<div>\n",
    "    <img src=\"images/image1.jpeg\" alt=\"Drawing\" align=center width=\"500\"/>\n",
    "</div>\n",
    "From above figure, for any decision boundary which separates two classes we can write:\n",
    "\n",
    "\\begin{equation}\n",
    "y_i (\\beta_0 + \\beta_1 x_1 + \\beta_2 x_2) > 0   \n",
    "\\label{eq:1} \\tag{1}\n",
    "\\end{equation}\n",
    "\n",
    "$\\forall i \\in \\{1, 2, 3, \\cdots, n\\}$ where $n$ is the number of training examples and $y_i = -1, 1$ are the class labels. \n",
    "\n",
    "We can generalize this for any $p$-dimensional feature space:\n",
    "\n",
    "\\begin{equation}\n",
    "y_i (\\beta_0 + \\beta_1 x_1 + \\beta_2 x_2 + \\cdots + \\beta_p x_p) > 0  \\qquad \\mathrm{or}, \\\\\n",
    "y_i (x^{T} \\beta + \\beta_0) > 0\n",
    "\\label{eq:2} \\tag{2}\n",
    "\\end{equation} \n",
    "\n",
    "__How to find the separating hyperplane?__\n",
    "\n",
    "1. Compute the perpendicular distance of each training example from given hyperplane\n",
    "2. The smallest of those distances is the minimal distance from the training examples from the hyperplane and is called __margin__\n",
    "3. Find the hyperplane with the largest margin\n",
    "\n",
    "We will work for the case of only two features to see what we meant.\n",
    "\n",
    "<div>\n",
    "    <img src=\"images/image2.jpeg\" alt=\"Drawing\" align=center width=\"500\"/>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As seen from above figure, the two classes are exactly separable with a hyperplane (a line) which is given by the solution of the optimization problem as $x^T \\beta + \\beta_0 = 0$. The band in the figure is $M$ units away from the hyperplane on either side, and hence $2M$ units wide. This band is called the __margin__. As we can see in the figure, it is the shortest perpendicular distance from the hyperplane to the training examples. The training examples which lie on the margin boundaries are called __support vectors__.\n",
    "\n",
    "The optimization condition for the Maximal Margin Classifier requires to maximize the margin between two classes. But it rarely happens that two classes can be exactly seperable in real world data. Hence for the case when two classes are not _exactly_ separable, no maximal margin classifier exists. For the non-separable classes, we have to use a __support vector classifier.__"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Support Vector Classifier\n",
    "Now suppose the classes are non-separable and there is some class overlap in the feature space. One way to deal with this is to still maximize $M$ while allowing some training examples to be wrongly classified. This would add robustness to the classifier since Maximal Margin classifier is extremely sensitive to change in the training examples near the margin boundary.\n",
    "\n",
    "<div>\n",
    "    <img src=\"images/image3.jpeg\" alt=\"Drawing\" align=center width=\"600\"/>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here we can see the case of class overlap. Again, the decision boundary is the heavy solid line while the lighter solid lines enclose the region of maximal margin of width $2M = \\left. 2\\,  \\middle/ \\mid\\mid\\beta\\mid\\mid \\right.$. \n",
    "- We define something called as _slack_ variables which tell us how much does each training example violate the margin\n",
    "- The data points labeled $\\epsilon_{i}^{*}$ are on the wrong side on margin by an amount $\\epsilon_{i}^{*} = M \\epsilon_{i}$\n",
    "- The points on the correct side of the margin will have $\\epsilon_{i}^{*} = 0$ (_no violation_)\n",
    "- If $ 0 < \\epsilon_{i} < 1$ this implies $0 < \\epsilon_{i}^{*} < M$. This means the training example is on the wrong side of the margin\n",
    "- If $ \\epsilon_{i} > 1$ this implies $\\epsilon_{i}^{*} > M$. This means the training example is on the wrong side of the hyperplane\n",
    "\n",
    "This whole idea can be represented mathematically by the optimization problem:\n",
    "\\begin{equation}\n",
    "    \\min \\mid\\mid\\beta\\mid\\mid \\mathrm{subject \\;  to} \\begin{cases} \n",
    "    y_i (x_i^T \\beta + \\beta_0) \\geq 1 - \\epsilon_i \\; \\forall i, \\\\\n",
    "    \\epsilon_i \\geq 0,\\; \\sum \\epsilon_i \\leq \\mathrm{constant}\n",
    "    \\end{cases}\n",
    "    \\label{eq:3} \\tag{3}\n",
    "\\end{equation}\n",
    "\n",
    "Computationally, the above equation can be written as \n",
    "\\begin{equation}\n",
    "    \\min_{\\beta,\\, \\beta_0} \\frac{1}{2} \\mid\\mid\\beta\\mid\\mid^2 + \\, C \\sum_{i=0}^{n} \\epsilon_i \\label{eq:4} \\tag{4}\n",
    "\\end{equation}\n",
    "subject to $\\epsilon_i \\geq 0,\\; y_i (x_i^T \\beta + \\beta_0) \\geq 1 - \\epsilon_i \\; \\forall i$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$C$ is a hyperparameter called the _cost parameter_. The case of separable classes (no class overlap) corresponds to $C = \\infty$. The following can be interpreted about parameter $C$:\n",
    "- It can be seen from Eq.(4) that the lower values of $C$ will make the margin larger and vice-versa. This makes sense since lowering $C$ will increase $\\sum_{i} \\epsilon_i$ (letting more training examples violating the margin)\n",
    "- Hence larger values of C focus attention more on (correctly classified) points near the decision boundary, while smaller values involve data further away. \n",
    "- Parameter $C$ can be thought of as a regularization parameter, where higher value of $C$ overfits the data (_more restrictive of margin violations_) and hence results in high variance and low bias. Similarly, lower value of $C$ underfits the data (high bias and low variance)\n",
    "\n",
    "<div>\n",
    "    <img src=\"images/image4.jpeg\" alt=\"Drawing\" align=center width=\"600\"/>\n",
    "</div>\n",
    "\n",
    "<div>\n",
    "    <img src=\"images/image5.jpeg\" alt=\"Drawing\" align=center width=\"600\"/>\n",
    "</div>\n",
    "\n",
    "In both the above figures, the solid dots refer to the support vectors which fall exactly on the margin ($\\epsilon_i =0$)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Classification with Non-linear Decision Boundaries and Support Vector Machines"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
